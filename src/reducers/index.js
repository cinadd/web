  
import { combineReducers } from 'redux'
import {
    GET_MOVIES_LIST
} from 'actions'

const moviesReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_MOVIES_LIST:
      return action.movies
    default:
      return state
  }
}

const rootReducer = combineReducers({
  moviesReducer
})

export default rootReducer