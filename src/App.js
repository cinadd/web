import React from 'react';
import Home from 'views/DefaultLayout';

import './App.css';


class App extends React.Component {
  constructor(props){
    super(props)
    this.state ={}
  }
  componentDidMount() {
  }

  render(){
    return (
      <div className="App">
        <Home />
       </div>
    );
  }
}

export default App;
