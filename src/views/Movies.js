import React from "react";
import JssProvider from "react-jss/lib/JssProvider";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import MovieCard from "components/movie/MovieCard";
import { Grid } from "@material-ui/core";

// import "./styles.css";

const muiBaseTheme = createMuiTheme();

const pics = [
  "https://inkino.imgix.net/1012/Event_12489/portrait_medium/MenInBlackInternational_1080.jpg",
  "https://inkino.imgix.net/1012/Event_12568/portrait_medium/Apollo11_1080u.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12519/portrait_medium/ToyStory4_1080t.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12457/portrait_medium/TheSecretLifeOfPets2_1080.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12473/portrait_medium/AnnabelleComesHome_1080.jpg?auto=format,compress",
  "https://inkino.imgix.net/1012/Event_12341/portrait_medium/Aurora_1080.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12435/portrait_medium/Tolkien_1080.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12519/portrait_medium/ToyStory4_1080t.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12457/portrait_medium/TheSecretLifeOfPets2_1080.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12473/portrait_medium/AnnabelleComesHome_1080.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12353/portrait_medium/RistoRappaajaJaPullistelija_1080.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12625/portrait_medium/Pavarotti_doc_1080.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12437/portrait_medium/Aladdin_1080.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12589/portrait_medium/AngelHasFallen_1080t.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12435/portrait_medium/Tolkien_1080.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12519/portrait_medium/ToyStory4_1080t.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12457/portrait_medium/TheSecretLifeOfPets2_1080.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12473/portrait_medium/AnnabelleComesHome_1080.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12353/portrait_medium/RistoRappaajaJaPullistelija_1080.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12625/portrait_medium/Pavarotti_doc_1080.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12437/portrait_medium/Aladdin_1080.jpg?auto=format,compress&w=300&h=450",
  "https://inkino.imgix.net/1012/Event_12589/portrait_medium/AngelHasFallen_1080t.jpg?auto=format,compress&w=300&h=450",
]


function Movies() {
  return (
    <JssProvider>
      <MuiThemeProvider
        theme={createMuiTheme({
          typography: {
            useNextVariants: true
          },
          overrides: MovieCard.getTheme(muiBaseTheme)
        })}
      >
        <Grid container spacing={2}>
            {pics.map((pic, i) => {
                      return (
                          <Grid key={i} item lg={3} sm={4} xs={6}>
                              <MovieCard src={pic}/>
                          </Grid>
                      )
                  })
            }
        </Grid>
      </MuiThemeProvider>
    </JssProvider>
  );
}
export default Movies;