import React from 'react';
import Box from '@material-ui/core/Box';
import Hidden from '@material-ui/core/Hidden';
import Container from '@material-ui/core/Container';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

import Header from 'components/layout/Header';
import Footer from 'components/layout/Footer';
import TabPanel from 'components/layout/TabPanel';
import { CssBaseline, Typography, Grid } from '@material-ui/core';
import Movies from './Movies';

function WithTheme() {
  const [value, setValue] = React.useState(0);

  function handleChange(event, newValue) {
    setValue(newValue);
  }

  return (
    <div>
        <Header value={value} handleChange={handleChange}/>
            <div>
            <Container>
                <Box my={14}>
                    <TabPanel value={value} index={0}>
                      <Hidden smDown>
                        <Grid container>
                          <Typography variant="h4">
                            <b>NOW IN CINEMAS</b>
                          </Typography>
                        </Grid>
                      </Hidden>
                      <Box my={2}>
                        <Movies />
                      </Box>

                    </TabPanel>
                    <TabPanel value={value} index={1}>
                      <Hidden smDown>
                        <Grid container>
                          <Typography variant="h4">
                            <b>SHOWTIMES</b>
                          </Typography>
                        </Grid>
                      </Hidden>
                      <Box my={2}>
                        <Movies />
                      </Box>
                    </TabPanel>
                    <TabPanel value={value} index={2}>
                      <Hidden smDown>
                        <Grid container>
                          <Typography variant="h4">
                            <b>COMING SOON</b>
                          </Typography>
                        </Grid>
                      </Hidden>
                      <Box my={2}>
                        <Movies />
                      </Box>
                    </TabPanel>
                </Box>
            </Container>
            </div>
        <Footer value={value} handleChange={handleChange} />
    </div>
  );
}

const theme = createMuiTheme({
  palette: {
    type: 'dark', // Switching the dark mode on is a single property value change.
  },
});

export default function DefaultLayout() {
  return (
    <ThemeProvider theme={theme}>
        <CssBaseline /> 
      <WithTheme />
    </ThemeProvider>
  );
}
