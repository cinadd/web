import React from "react";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";

const MovieCard = (props) => (
  <Card className={"MuiNewsCard--02"}>
    <CardMedia
      component={"img"}
      className={"MuiCardMedia-root"}
      src={props.src}
    />
    <CardContent className={"MuiCardContent-root"}>
      <Typography
        className={"MuiTypography--heading"}
        color={"inherit"}
        variant={"h3"}
        gutterBottom
      >
        {/* Space */}
      </Typography>
      <Typography className={"MuiTypography--subheading"} color={"inherit"}>
        {/* The space between the stars and galaxies is largely empty. */}
      </Typography>
      <Typography
        className={"MuiTypography--explore"}
        color={"inherit"}
        variant={"caption"}
      >
        <Link color={"inherit"} underline={"none"}>
          {" "}
        </Link>
      </Typography>
    </CardContent>
  </Card>
);

MovieCard.getTheme = muiBaseTheme => ({
  MuiCard: {
    root: {
      "&.MuiNewsCard--02": {
        cursor: "pointer",
        maxWidth: 304,
        margin: "auto",
        position: "relative",
        transition: "0.3s cubic-bezier(.47,1.64,.41,.8)",
        boxShadow: "0 16px 40px -12.125px rgba(0,0,0,0.3)",
        borderRadius: 0,
        "&:hover": {
          transform: "scale(1.04)",
          boxShadow: "0 4px 20px 0 rgba(0,0,0,0.12)"
        },
        "& button": {
          marginLeft: 0
        },
        "& .MuiCardMedia-root": {
          height: "100%"
        },
        "& .MuiCardContent-root": {
          position: "absolute",
          bottom: 0,
          padding: muiBaseTheme.spacing(3),
          color: muiBaseTheme.palette.common.white,
          textAlign: "center",
          "& .MuiTypography--subheading": {
            lineHeight: 1.8,
            letterSpacing: 0.5,
            marginBottom: "40%"
          },
          "& .MuiTypography--explore": {
            marginBottom: 16,
            transition: "0.3s cubic-bezier(.47,1.64,.41,.8)",
            letterSpacing: 2
          }
        }
      }
    }
  }
});
MovieCard.metadata = {
  name: "Movie Card",
  description: "Display overview of a movie"
};


export default MovieCard;
