import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import RestoreIcon from '@material-ui/icons/Restore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import { Hidden } from '@material-ui/core';

const useStyles = makeStyles( {
    root: {
      // width: 500
    },
    bottomNav: {
      fontSize: "50px"
    }

});
export default function SimpleBottomNavigation(props) {
  const classes = useStyles();

  return (
    <Hidden mdUp>
      <BottomNavigation
        value={props.value}
        onChange={props.handleChange}
        showLabels
        className={classes.root}
      >
        <BottomNavigationAction className={classes.bottomNav} label="Movies" icon={<RestoreIcon />} />
        <BottomNavigationAction className={classes.bottomNav} label="Showtimes" icon={<FavoriteIcon />} />
        <BottomNavigationAction className={classes.bottomNav} label="Coming Soon" icon={<LocationOnIcon />} />
      </BottomNavigation>
    </Hidden>
  );
}