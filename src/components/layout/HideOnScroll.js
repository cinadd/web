import React from 'react';
import PropTypes from 'prop-types';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';


    
HideOnScroll.propTypes = {
    children: PropTypes.element.isRequired,
};

export default function HideOnScroll(props) {
    const { children } = props;
    const trigger = useScrollTrigger({ target: undefined });

    return (
        <Slide appear={false} direction="down" in={!trigger}>
            {children}
        </Slide>
    );
}
