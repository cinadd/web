import Api from 'services/api.js'

export const GET_MOVIES_LIST = 'GET_MOVIES_LIST'
export const GET_MOVIE_INFO = 'GET_MOVIE_INFO'
export const GET_CINEMA_SCHEDULE = 'GET_CINEMA_SCHEDULES'
export const GET_MOVIE_SCHEDULE = 'GET_MOVIE_SCHEDULE'
export const GET_COMING_SOON = 'GET_COMING_SOON'

export const getMoviesLst = movie => ({
    type: GET_MOVIES_LIST,
    movies: Api.find('Movies', null, {})
                .then( respoonse => respoonse)
                .catch(error => error),
})